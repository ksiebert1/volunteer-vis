import Vue from 'vue';
import Visualisation from '@/components/Visualisation';

describe('Visualisation.vue', () => {
  it('should render correct contents', () => {
    const Constructor = Vue.extend(Visualisation);
    const vm = new Constructor().$mount();
    expect(vm.$el.querySelector('.hello h1').textContent)
      .toEqual('Welcome to Your Vue.js App');
  });
});
